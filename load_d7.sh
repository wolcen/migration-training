#!/usr/bin/env bash
# Allows one to download and load databases and drupal files.
# Adjust db* if necessary to use elsewhere (outside training DrupalVM).
#
# See also: http://docs.drupalvm.com/en/latest/configurations/databases-mysql/

# Places files in local $cache/ folder to save on repeat downloads.

proj_root="$(dirname "$(readlink -f "$0")")"
# Expected location...should be set by above, but if moved to e.g. /usr/local/bin
# then this will need a fixed value instead:
#proj_root="/var/www/drupalvm"

filesurl="https://raw.githubusercontent.com/dinarcon/ud_d8_upgrade/master"
files_dir="${proj_root}/drupal7/sites/default"       #drupal 7 target files
cache="${proj_root}/db_cache"    #cache folder
# Drupal settings (by default, only uses db_user/pwd/and db from $1)
db_user="drupal"      #user
db_password="drupal"  #pwd
db="drupal7"          #name

# These are only used if you must customize your sqlcmd/admincmd
db_host="localhost"   #host
db_port="3306"        #port
db_socket="/var/run/mysqld/mysqld.sock" #socket path
db_protocol="socket"  #connect method: tcp, socket, pipe, memory

db_admin_user="root"
db_admin_password=""

# SQL command aliases
#admin="mysqladmin --password=${db_admin_password} --user=${db_admin_user} --host=${db_host} --port=${db_port} --socket=${db_socket} --protocol=${db_protocol}"
admin="mysqladmin"
#sqlcmd="mysql --password=${db_admin_password} --user=${db_admin_user} --host=${db_host} --port=${db_port} --socket=${db_socket} --protocol=${db_protocol}"
sqlcmd="mysql"

# Abort on any errors.
set -e

# Show a friendly explanation.
print_usage() {
cat <<-USAGE
This script drops a local database and re-loads a specified source.
Usage: ${0##*/} source-selection [-y]
Example:
${0##*/} drupal7
USAGE
}
# Ensure we are running as root:
if [ $(id -u) != 0 ]; then
  printf "ERROR: You must run this as root with \`sudo -i\`.\n\n"
  print_usage
  exit 1
fi

# Input validation...(need one arg)
confirm="FALSE"
while [[ $# -gt 0 ]]
do
case "$1" in
    -y|--yes)
    confirm="TRUE"
    shift
    ;;
    *)
    src_select="$1"
    shift
    ;;
esac
done
if [ -z "${src_select}" ]; then
  echo "ERROR: Source missing - specify a source name."
  print_usage
  exit 1
fi
# Set up other run-time vars
sqlgz="${src_select}.sql.gz"                        #sql gzip
filesgz="files.gz"
target="${cache}/${sqlgz}"                 #expected cache location
target_files="${cache}/${src_select}-files.gz"      #expected cache location
echo "${target_files}"
# Check cache (download) folder exists
if [ ! -d ${cache} ]; then
  mkdir -p ${cache}
  echo "Note: created ${cache} folder."
fi

# Is the requested db source already local?
# also specify target - local path to source gz.
acquire_msg=''
url=''
if [ ! -f ${target} ]; then
  url="${filesurl}/${src_select}/${sqlgz}"
  acquire_msg="\n- download from ${url}, "
fi
# Same deal for the files tar.
if [ ! -f ${target_files} ]; then
  url_files="${filesurl}/${src_select}/${filesgz}"
  acquire_msg="${acquire_msg}\n- download files from ${url_files}, "
fi

# Function that downloads/drops/loads...
load_db_and_files() {
  if [ ! -z ${url} ]; then
    curl --fail --output "${target}" "${url}"
  fi
  if [ ! -z ${url_files} ]; then
    echo "HELP!"
    echo "curl --fail --output \"${target_files}\" \"${url_files}\""
    curl --fail --output "${target_files}" "${url_files}"
  fi

  #echo "Using mysql command: ${sqlcmd}"
  #echo "Using sql admin command: ${admin}"
  echo ">> Dropping ${db}"
  ${admin} -f drop ${db}

  echo ">> Creating ${db}"
  ${admin} -f create ${db}

  #echo "${sqlcmd} -e \"GRANT ALL ON ${db}.* TO ${db_user}@${db_host} IDENTIFIED BY \"${db_password}\""
  grant="GRANT ALL ON ${db}.* TO '${db_user}'@'%' IDENTIFIED BY '${db_password}' WITH GRANT OPTION;"
  echo ">> Granting permission with (must be root): ${sqlcmd} -e ${grant}"
  set +e
  ${sqlcmd} -e "${grant}"
  set -e
  # Alternative method:
  # sqlcmd -D mysql -e <<"SQL_CMD"
  # DROP DATABASE IF EXISTS ${db};
  # CREATE DATABASE ${db};
  # GRANT ALL ON ${db}.* TO ${db_user}@${db_host} IDENTIFIED BY '${db_password}';
  # FLUSH PRIVILEGES;
  # SQL_CMD

  echo ">> Loading database..."
  #${admin} reload
  zcat ${target} | ${sqlcmd} ${db}
  echo ">> Database loaded!"

  echo ">> Extracting files..."
  if [ ! -d ${files_dir} ]; then
    mkdir -p ${files_dir}
    echo "Note: created ${files_dir} folder."
    chmod 775 ${files_dir}
  fi

  cd ${files_dir}
  tar xvzf ${target_files}
  find ${files_dir} -type d -print0 | xargs -0 chmod 775
  find ${files_dir} -type f -print0 | xargs -0 chmod 664
}

# Confirm before execution
printf "Going to \n- [drop and re-]create database ${db}${acquire_msg}\n- load from ${sqlgz}\n- extract files to ${files_dir}.\n"
if [ "${confirm}" == "FALSE" ]; then
  read -p "Continue y/n? " choice
else
  choice="Y"
fi
case "$choice" in
  y|Y ) load_db_and_files;;
  n|N ) echo "Cancelled: User cancel";;
  * ) echo "Cancelled: Invalid option";;
esac
